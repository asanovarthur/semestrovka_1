﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace Semestrovka_1
{
    public class Edge
    {
        public int Item1 { get; set; }
        public int Item2 { get; set; }
        public Edge Next { get; set; }

        public override bool Equals(object obj)
        {
            if (!(obj is Edge)) return false;
            var edge = obj as Edge;
            return ((Item1 == edge.Item1 && Item2 == edge.Item2)
                || (Item1 == edge.Item2 && Item2 == edge.Item1));
        }
    }

    public class MyLinkedList : IEnumerable
    {
        public Edge First { get; set; }
        public Edge Last { get; set; }
        public int Count { get; private set; }

        public MyLinkedList()
        {
            First = Last = null;
        }

        public void AddToEnd(Edge edge)
        {
            if (First == null)
            {
                First = Last = edge;

            }
            else
                Last.Next = edge;
            Last = edge;
            Count++;
        }

        public void Remove(Edge edge)
        {
            if(edge.Equals(First))
            {
                First = First.Next;
            }
            else
            {
                var el = FindEdge(edge);
                var prevEl = FindElementBeforeItem(edge);
                prevEl.Next = el.Next;
            }
            Count--;
        }

        private Edge FindElementBeforeItem(Edge edge)
        {
            var i = First;
            while (!i.Next.Equals(edge))
            {
                i = i.Next;
            }
            return i;
        }

        private Edge FindEdge(Edge edge)
        {
            if (!Contains(edge))
                throw new InvalidOperationException();
            var i = First;
            while (!i.Equals(edge))
            {
                i = i.Next;
            }
            return i;
        }

        public bool Contains(Edge edge)
        {
            var i = First;
            while (i != null)
            {
                if (i.Equals(edge))
                    return true;
                i = i.Next;
            }
            return false;
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public IEnumerator GetEnumerator()
        {
            var current = First;
            while (current != null)
            {
                yield return current;
                current = current.Next;
            }
        }
    }

    public class GraphCode
    {
        MyLinkedList edgeList = new MyLinkedList();

        public int StringLength { get; set; }

        private int[][] FromMiToNormal(int[][] mi)
        {
            var result = new int[mi[0].Length][];
            for (int i = 0; i < result.Length; i++)
            {
                result[i] = new int[mi.Length];
                for (int j = 0; j < mi.Length; j++)
                {
                    result[i][j] = mi[j][i];
                    var check2 = result[i][j];
                }
            }
            return result;
        }

        private int[][] FromNormalToMi(int[][] normal)
        {
            int counter = 0;
            foreach (Edge edge in edgeList)
            {
                normal[counter] = new int[StringLength];
                normal[counter][edge.Item1 - 1] = 1;
                normal[counter][edge.Item2 - 1] = 1;
                counter++;
            }
            if (normal.Length == 0) return new int[][] { };
            var resultMi = new int[normal[0].Length][];
            for (int i = 0; i < resultMi.Length; i++)
            {
                resultMi[i] = new int[normal.Length];
            }
            for (int i = 0; i < resultMi.Length; i++)
            {
                for (int j = 0; j < normal.Length; j++)
                {
                    resultMi[i][j] = normal[j][i];
                    var check1 = resultMi[i][j];
                }
            }
            return resultMi;
        }

        public GraphCode(int[][] mi)
        {
            var normalMi = FromMiToNormal(mi);
            StringLength = normalMi[0].Length;
            for (int i = 0; i < normalMi.Length; i++)
            {
                int start = 0;
                int end = 0;
                for (int j = 0; j < StringLength; j++)
                {
                    if (normalMi[i][j] == 1)
                    {
                        if (start == 0) start = j + 1;
                        else
                        {
                            end = j + 1;
                            edgeList.AddToEnd(new Edge() { Item1 = start, Item2 = end });
                        }
                    }
                }
            }
        }

        public int[][] GetMI()
        {
            var resultNormal = new int[edgeList.Count][];
            var resultMi = FromNormalToMi(resultNormal);
            return resultMi;
        }

        public void Insert(int i, int j)
        {
            if (!(i >= 0 && i <= StringLength) || !(j >= 0 && j <= StringLength))
                throw new ArgumentException("Wrong number for vertex(es)");
            if (edgeList.Contains(new Edge() { Item1 = i, Item2 = j })
                || edgeList.Contains(new Edge() { Item1 = j, Item2 = i }))
                throw new InvalidOperationException("Edge already exists");
            edgeList.AddToEnd(new Edge() { Item1 = i, Item2 = j });
        }

        public void Delete(int i, int j)
        {
            if (!(i >= 0 && i <= StringLength) || !(j >= 0 && j <= StringLength))
                throw new ArgumentException("Wrong number for vertex(es)");
            var countBefore = edgeList.Count;
            if (edgeList.Contains(new Edge() { Item1 = i, Item2 = j }))
                edgeList.Remove(new Edge() { Item1 = i, Item2 = j });
            if (edgeList.Contains(new Edge() { Item1 = j, Item2 = i }))
                edgeList.Remove(new Edge() { Item1 = j, Item2 = i });
            var countAfter = edgeList.Count;
            if (countAfter == countBefore)
                throw new InvalidOperationException("Edge is not in a graph");
        }

        public MyLinkedList GetEdgesWithNode(int i)
        {
            if (!(i >= 0 && i <= StringLength))
                throw new ArgumentException("Wrong number for vertex");
            var result = new MyLinkedList();
            foreach (Edge edge in edgeList)
            {
                if ((edge.Item1 == i) || (edge.Item2 == i))
                    if (!result.Contains(new Edge() { Item1 = edge.Item1, Item2 = edge.Item2 })
                        && (!result.Contains(new Edge() { Item1 = edge.Item2, Item2 = edge.Item1 })))
                        result.AddToEnd(new Edge() { Item1 = edge.Item1, Item2 = edge.Item2 });
            }
            return result;
        }

        public void Modify(int i)
        {
            var toDelete = new MyLinkedList();
            foreach (Edge edge in edgeList)
            {
                if ((edge.Item1 == i) || (edge.Item2 == i))
                    toDelete.AddToEnd(edge);
            }
            var tmp = toDelete.First;
            while (true)
            {
                if (toDelete.Count == 1)
                {
                    edgeList.Remove(tmp);
                    break;
                }
                else
                {
                    tmp = tmp.Next;
                }
            }
        }

        public List<int> Show(int m)
        {
            var incidenceDegrees = new int[StringLength + 1];
            foreach (Edge edge in edgeList)
            {
                incidenceDegrees[edge.Item1]++;
                incidenceDegrees[edge.Item2]++;
            }
            var result = new List<int>();
            for (int i = 1; i < incidenceDegrees.Length; i++)
            {
                if (incidenceDegrees[i] > m)
                    result.Add(i);
            }
            return result;
        }
    }


    class Program
    {
        static void Main()
        {
            int[][] mi = new int[][] { new int[] { 1, 1 }, new int[] { 1, 0 }, new int[] { 0, 1 } };
            var result = new GraphCode(mi);
            result.Modify(2);
            var resultMI = result.GetMI();

            foreach (var e in resultMI)
                foreach (var a in e)
                    Console.Write(a + " ");

            
        }
    }
}