﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;

namespace Semestrovka_1.Tests
{
    [TestClass]
    public class UnitTest1
    {
        static int[][] mi = new int[][] { new int[] { 1, 1 }, new int[] { 1, 0 }, new int[] { 0, 1 } };

        [TestMethod]
        public void GetOriginalMI()
        {
            var result = new GraphCode(mi).GetMI();
            for (int i = 0; i < mi.Length; i++)
                for (int j = 0; j < mi[i].Length; j++)
                    Assert.AreEqual(mi[i][j], result[i][j]);
        }

        [TestMethod]
        public void GetMIAfterInsert()
        {
            var newGraph = new GraphCode(mi);
            newGraph.Insert(2, 3);
            var result = newGraph.GetMI();
            var expected = new int[][] { new int[] { 1, 1, 0 }, new int[] { 1, 0, 1 }, new int[] { 0, 1, 1 } };
            for (int i = 0; i < expected.Length; i++)
                for (int j = 0; j < expected[i].Length; j++)
                    Assert.AreEqual(expected[i][j], result[i][j]);
        }

        [TestMethod]
        public void GetMIAfterDelete()
        {
            var newGraph = new GraphCode(mi);
            newGraph.Delete(1, 3);
            var result = newGraph.GetMI();
            var expected = new int[][] { new int[] { 1 }, new int[] { 1 }, new int[] { 0 } };
            for (int i = 0; i < expected.Length; i++)
                for (int j = 0; j < expected[i].Length; j++)
                    Assert.AreEqual(expected[i][j], result[i][j]);
        }

        [TestMethod]
        public void GetEdgesWithNormalNode()
        {
            var result = new GraphCode(mi).GetEdgesWithNode(3);
            var expected = new MyLinkedList();
            expected.AddToEnd(new Edge() { Item1 = 1, Item2 = 3 });
            bool toCheck = true;
            if (expected.Count != result.Count) toCheck = false;
            foreach(Edge edge in expected)
            {
                if (!result.Contains(edge)) toCheck = false;
            }
            Assert.AreEqual(true, toCheck);
        }

        [TestMethod]
        public void GetMIAfterModifying()
        {
            var result = new GraphCode(mi);
            result.Modify(2);
            var resultMI = result.GetMI();
            var expected = new int[][] { new int[] { 1 }, new int[] { 0 }, new int[] { 1 } };
            for (int i = 0; i < expected.Length; i++)
                for (int j = 0; j < expected[i].Length; j++)
                    Assert.AreEqual(expected[i][j], resultMI[i][j]);
        }

        [TestMethod]
        public void ShowWithNormalDegree()
        {
            var graph = new GraphCode(mi);
            var expected = new List<int>() { 1 };
            var result = graph.Show(1);
            for (int i = 0; i < expected.Count; i++)
                Assert.AreEqual(expected[i], result[i]);
        }
    }
}
